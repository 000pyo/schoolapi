/* eslint-disable func-names */
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('teachers', table => {
      table.string('user').primary();
      table.string('name');
    }),
    knex.schema.createTable('students', table => {
      table.string('user').primary();
      table.string('name');
      table.string('status');
    }),
    knex.schema.createTable('records', table => {
      table
        .string('teacher')
        .references('teachers.user')
        .notNullable();
      table
        .string('student')
        .references('students.user')
        .notNullable();
      table.unique(['teacher', 'student']);
    })
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('records'),
    knex.schema.dropTable('teachers'),
    knex.schema.dropTable('students')
  ]);
};
