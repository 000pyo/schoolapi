import request from 'supertest';
import { assert } from 'chai';

import app from '../src/app';
import { knex } from '../src/util/dbutil';

const teacher = {
  user: 'teacherw@example.com',
  name: 'W'
};

before(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

after(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

/**
 * Testing POST teacher
 */
describe('POST /api/maintain/teacher', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/teacher')
      .send({ teacher })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Testing POST teacher with wrong field
 */
describe('POST /api/maintain/teacher - invalid field', () => {
  it('response with code 400', done => {
    request(app)
      .post('/api/maintain/teacher')
      .send({ teacher: { user: 'asd', name1: '123' } })
      .expect('Content-Type', /json/)
      .expect(400)
      .end(done);
  });
});

/**
 * Testing POST duplicate teacher
 */
describe('POST /api/maintain/teacher - duplicate', () => {
  it('response with code 500', done => {
    request(app)
      .post('/api/maintain/teacher')
      .send({ teacher })
      .expect('Content-Type', /json/)
      .expect(500)
      .end(done);
  });
});

/**
 * Testing GET list of teacher
 */
describe('GET /api/maintain/teacher', () => {
  it('response with list of teacher, code 200', done => {
    request(app)
      .get('/api/maintain/teacher')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res =>
        assert.exists(res.body.teachers, 'teachers should exist in body')
      )
      .end(done);
  });
});

/**
 * Testing GET not exist teacher
 */
describe('GET /api/maintain/teacher/:id - not exist', () => {
  it('response with list of teacher, code 404', done => {
    request(app)
      .get(`/api/maintain/teacher/123`)
      .expect('Content-Type', /json/)
      .expect(404)
      .end(done);
  });
});

/**
 * Testing GET teacher
 */
describe('GET /api/maintain/teacher/:id', () => {
  it('response with teacher, code 200', done => {
    request(app)
      .get(`/api/maintain/teacher/${teacher.user}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        assert.equal(
          res.body.teacher.user,
          teacher.user,
          `response should be the same user`
        );
      })
      .end(done);
  });
});

/**
 * Testing DELETE not exist teacher
 */
describe('DELETE /api/maintain/teacher/:id - not exist', () => {
  it('response with code 404', done => {
    request(app)
      .get(`/api/maintain/teacher/1234`)
      .expect('Content-Type', /json/)
      .expect(404)
      .end(done);
  });
});

/**
 * Testing DELETE teacher
 */
describe('DELETE /api/maintain/teacher/:id', () => {
  it('response with code 200', done => {
    request(app)
      .delete(`/api/maintain/teacher/${teacher.user}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(done);
  });
});
