import request from 'supertest';
import { assert } from 'chai';

import app from '../src/app';
import { knex } from '../src/util/dbutil';

const teachers = [
  {
    user: 'teachera@example.com',
    name: 'A'
  },
  {
    user: 'teacherb@example.com',
    name: 'B'
  }
];

const students = [
  {
    user: 'studenta@example.com',
    name: 'A',
    status: ''
  },
  {
    user: 'studentb@example.com',
    name: 'B',
    status: ''
  },
  {
    user: 'studentc@example.com',
    name: 'C',
    status: 'suspended'
  }
];

before(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

after(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

/**
 * Add Student A
 */
describe('Add Student A', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student: students[0] })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Add Student B
 */
describe('Add Student B', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student: students[1] })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Add Student C
 */
describe('Add Student C', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student: students[2] })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Add Teacher A
 */
describe('Add Teacher A', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/teacher')
      .send({ teacher: teachers[0] })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Add Teacher B
 */
describe('Add Teacher B', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/teacher')
      .send({ teacher: teachers[1] })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * =========
 * Test
 * =========
 */

/**
 * Register with non exist teacher
 */
describe('POST /api/register: Register non exist teacher', () => {
  it('response with code 404', done => {
    request(app)
      .post('/api/register')
      .send({ teacher: '123', students: [students[0].user] })
      .expect(404)
      .end(done);
  });
});

/**
 * Register with non exist student
 */
describe('POST /api/register: Register non exist student', () => {
  it('response with code 404', done => {
    request(app)
      .post('/api/register')
      .send({
        teacher: teachers[0].user,
        students: ['123', students[0].user]
      })
      .expect(404)
      .end(done);
  });
});

/**
 * Register with invalid body
 */
describe('POST /api/register: Invalid body', () => {
  it('response with code 400', done => {
    request(app)
      .post('/api/register')
      .send({ teachers: '123', students: [students[0].user] })
      .expect(400)
      .end(done);
  });
});

/**
 * Register Student A, B with Teacher A
 */
describe('POST /api/register: Register SA, SB for TA', () => {
  it('response with code 204', done => {
    request(app)
      .post('/api/register')
      .send({
        teacher: teachers[0].user,
        students: [students[0].user, students[1].user]
      })
      .expect(204)
      .end(done);
  });
});

/**
 * Register Student A with Teacher B
 */
describe('POST /api/register: Register SA to TB', () => {
  it('response with code 204', done => {
    request(app)
      .post('/api/register')
      .send({
        teacher: teachers[1].user,
        students: [students[0].user]
      })
      .expect(204)
      .end(done);
  });
});

/**
 * Get Common Students - not exist teacher
 */
describe('GET /api/commonstudents: not exist teacher', () => {
  it('response with empty students and code 200', done => {
    request(app)
      .get('/api/commonstudents')
      .query({
        teacher: '123'
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        assert.equal(res.body.students.length, 0, 'students length');
      })
      .end(done);
  });
});

/**
 * Get Common Students - Teacher A
 */
describe('GET /api/commonstudents: TA', () => {
  it('response with list student A & B code 200', done => {
    request(app)
      .get('/api/commonstudents')
      .query({
        teacher: teachers[0].user
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        assert.equal(res.body.students[0], students[0].user, 'student A');
        assert.equal(res.body.students[1], students[1].user, 'student B');
      })
      .end(done);
  });
});

/**
 * Get Common Students - Teacher A & B
 */
describe('GET /api/commonstudents: TA & TB', () => {
  it('response with list student A code 200', done => {
    request(app)
      .get(
        `/api/commonstudents?teacher=${teachers[0].user}&teacher=${
          teachers[1].user
        }`
      )
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        assert.equal(res.body.students.length, 1, 'students length');
        assert.equal(res.body.students[0], students[0].user, 'student A');
      })
      .end(done);
  });
});

/**
 * Notification with not exist teacher
 */
describe('POST /api/retrievefornotifications: not exist', () => {
  it('response with code 404', done => {
    request(app)
      .post('/api/retrievefornotifications')
      .send({
        teacher: '123',
        notification: 'Test'
      })
      .expect('Content-Type', /json/)
      .expect(404)
      .end(done);
  });
});

/**
 * Notification with invalid body
 */
describe('POST /api/retrievefornotifications: not exist', () => {
  it('response with code 400', done => {
    request(app)
      .post('/api/retrievefornotifications')
      .send({
        teacher: teachers[0].user,
        notifications: 'Test'
      })
      .expect('Content-Type', /json/)
      .expect(400)
      .end(done);
  });
});

/**
 * Notification - Teacher A
 */
describe('POST /api/retrievefornotifications: TA', () => {
  it('response with code 200', done => {
    request(app)
      .post('/api/retrievefornotifications')
      .send({
        teacher: teachers[0].user,
        notification: 'Test'
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        const { recipients } = res.body;
        assert.equal(recipients[0], students[0].user, 'student A');
        assert.equal(recipients[1], students[1].user, 'student B');
      })
      .end(done);
  });
});

/**
 * Notification - Teacher A @ Student C
 */
describe('POST /api/retrievefornotifications: TA@SC', () => {
  it('response with code 200', done => {
    request(app)
      .post('/api/retrievefornotifications')
      .send({
        teacher: teachers[0].user,
        notification: `Test @${students[2].user}`
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        const { recipients } = res.body;
        assert.equal(recipients[0], students[0].user, 'student A');
        assert.equal(recipients[1], students[1].user, 'student B');
        assert.equal(recipients[2], students[2].user, 'student C');
      })
      .end(done);
  });
});

/**
 * Suspend - not exist student
 */
describe('POST /api/suspend: not exist', () => {
  it('response with code 404', done => {
    request(app)
      .post('/api/suspend')
      .send({
        student: '123'
      })
      .expect(404)
      .end(done);
  });
});

/**
 * Suspend - invalid body
 */
describe('POST /api/suspend: invalid body', () => {
  it('response with code 400', done => {
    request(app)
      .post('/api/suspend')
      .send({
        students: students[1].user
      })
      .expect(400)
      .end(done);
  });
});

/**
 * Suspend - student B
 */
describe('POST /api/suspend: SB', () => {
  it('response with code 204', done => {
    request(app)
      .post('/api/suspend')
      .send({
        student: students[1].user
      })
      .expect(204)
      .end(done);
  });
});

/**
 * Notification - Teacher A @ Student C, Student B suspended
 */
describe('POST /api/retrievefornotifications: TA@SC, SC suspened', () => {
  it('response with code 200', done => {
    request(app)
      .post('/api/retrievefornotifications')
      .send({
        teacher: teachers[0].user,
        notification: `Test @${students[2].user}`
      })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        const { recipients } = res.body;
        assert.equal(recipients[0], students[0].user, 'student A');
        assert.equal(recipients[1], students[1].user, 'student C');
      })
      .end(done);
  });
});
