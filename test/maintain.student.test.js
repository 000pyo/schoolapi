import request from 'supertest';
import { assert } from 'chai';

import app from '../src/app';
import { knex } from '../src/util/dbutil';

const student = {
  user: 'studentv@example.com',
  name: 'V',
  status: ''
};

before(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

after(done => {
  knex.migrate.rollback().then(() => {
    knex.migrate.latest().then(() => {
      done();
    });
  });
});

/**
 * Testing POST student
 */
describe('POST /api/maintain/student', () => {
  it('response with code 201', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student })
      .expect('Content-Type', /json/)
      .expect(201)
      .end(done);
  });
});

/**
 * Testing POST student with wrong field
 */
describe('POST /api/maintain/student - invalid body', () => {
  it('response with code 400', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student: { user: 'asd', name1: '123' } })
      .expect('Content-Type', /json/)
      .expect(400)
      .end(done);
  });
});

/**
 * Testing POST duplicate student
 */
describe('POST /api/maintain/student - duplicate', () => {
  it('response with code 500', done => {
    request(app)
      .post('/api/maintain/student')
      .send({ student })
      .expect('Content-Type', /json/)
      .expect(500)
      .end(done);
  });
});

/**
 * Testing GET list of student
 */
describe('GET /api/maintain/student', () => {
  it('response with list of student, code 200', done => {
    request(app)
      .get('/api/maintain/student')
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res =>
        assert.exists(res.body.students, 'students should exist in body')
      )
      .end(done);
  });
});

/**
 * Testing GET not exist student
 */
describe('GET /api/maintain/student/:id - not exist', () => {
  it('response with list of student, code 404', done => {
    request(app)
      .get(`/api/maintain/student/123`)
      .expect('Content-Type', /json/)
      .expect(404)
      .end(done);
  });
});

/**
 * Testing GET student
 */
describe('GET /api/maintain/student/:id', () => {
  it('response with student, code 200', done => {
    request(app)
      .get(`/api/maintain/student/${student.user}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => {
        assert.equal(
          res.body.student.user,
          student.user,
          `response should be the same user`
        );
      })
      .end(done);
  });
});

/**
 * Testing DELETE not exist student
 */
describe('DELETE /api/maintain/student/:id - not exist', () => {
  it('response with code 404', done => {
    request(app)
      .get(`/api/maintain/student/1234`)
      .expect('Content-Type', /json/)
      .expect(404)
      .end(done);
  });
});

/**
 * Testing DELETE student
 */
describe('DELETE /api/maintain/student/:id', () => {
  it('response with code 200', done => {
    request(app)
      .delete(`/api/maintain/student/${student.user}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end(done);
  });
});
