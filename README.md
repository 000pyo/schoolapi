# School API

[Node.js](https://nodejs.org) with [Express.js](https://expressjs.com) and [Objection.js](https://vincit.github.io/objection.js/) project for School API.

For the first time, install all dependencies with:

```yarn
yarn install
```

Then, set up environment variables. See [Environment Variable Section](#environment-variables).

Then, run knex migration script with

```yarn
yarn migrate
```

Finally, you can start development with

```yarn
yarn dev
```

For non-production environment, you can access to Swagger UI documentation under root path when it is running.

## Scripts

In the project directory, you can run:

| Script        | Description                            |
| ------------- | -------------------------------------- |
| yarn dev      | Start app with nodemon from src/app.js |
| yarn build    | Build app to dist/                     |
| yarn start    | Start node script from dist/           |
| yarn test     | Start test script                      |
| yarn lint     | Linting files with ESLint              |
| yarn migrate  | Start knex migration script            |
| yarn rollback | Rollback knex migration                |

## Environment Variables

This project will read values from node environment as follow

| Var     | Default   | Description                 |
| ------- | --------- | --------------------------- |
| PORT    | 8081      | Port for this app to run on |
| DB_URL  | localhost | URL to MySQL                |
| DB_PORT | 3306      | Port of MySQL               |
| DB_USER | user      | Username for MySQL          |
| DB_PASS | user1234  | Password of MySQL user      |
| DB_NAME | schoolapi | DB Name                     |

On development, you can create .env file at the root of the project to simulate the node environment variables without actually setting it.

Below is sample .env file

```.env
PORT=8081

DB_HOST=localhost
DB_PORT=3306
DB_USER=user
DB_PASS=user1234
DB_NAME=schoolapi
```
