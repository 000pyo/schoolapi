FROM node:latest
ENV NODE_ENV production
ENV PORT 8081
ENV DB_URL mysql
ENV DB_PORT 3306
ENV DB_NAME schoolapi
WORKDIR /usr/src/app
COPY package.json /usr/src/app
COPY yarn.lock /usr/src/app
RUN yarn install --production --silent && mv node_modules ../
COPY dist dist
EXPOSE 8081
CMD yarn start