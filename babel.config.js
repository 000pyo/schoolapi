const presets = ['@babel/env'];
const plugins = ['inline-dotenv', '@babel/plugin-transform-runtime'];

module.exports = { presets, plugins };
