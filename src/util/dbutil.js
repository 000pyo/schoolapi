import log4js from 'log4js';
import { Model } from 'objection';
import Knex from 'knex';

const logger = log4js.getLogger();

const DB_HOST = process.env.DB_HOST || 'localhost';
const DB_PORT = process.env.DB_PORT || '3306';
const DB_USER = process.env.DB_USER || 'user';
const DB_PASS = process.env.DB_PASS || 'user1234';
const DB_NAME = process.env.DB_NAME || 'schoolapi';

const dbConfig = {
  client: 'mysql',
  connection: {
    host: DB_HOST,
    port: DB_PORT,
    user: DB_USER,
    password: DB_PASS,
    database: DB_NAME
  }
};

const knex = Knex(dbConfig);

const initModel = () => {
  Model.knex(knex);
  logger.debug('Model connected with knex');
};

export default initModel;
export { knex };
