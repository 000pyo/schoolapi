import { Router } from 'express';

import main from '../components/main';
import maintain from '../components/maintain';

const router = Router();

router.use('/', main);
router.use('/maintain', maintain);

module.exports = router;
