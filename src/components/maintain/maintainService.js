import log4js from 'log4js';
import { Teacher, Student } from './maintainModel';

const logger = log4js.getLogger();

const getErrorMessage = err => {
  const { message, code, sqlMessage } = err;
  logger.error(code, message);

  let rescode = code;
  let resmessage = message;
  logger.debug(code);
  switch (code) {
    case 'ER_BAD_FIELD_ERROR':
      rescode = 400;
      resmessage = sqlMessage;
      break;
    case 'ER_DUP_ENTRY':
      rescode = 500;
      resmessage = sqlMessage;
      break;
    case 404:
      break;
    default:
      rescode = 500;
      break;
  }

  return { code: rescode, message: resmessage };
};

const getTeachers = async (req, res) => {
  try {
    const result = await Teacher.query();
    res.status(200).json({
      teachers: result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const addTeacher = async (req, res) => {
  try {
    const { teacher } = req.body;
    const result = await Teacher.query().insert(teacher);
    res.status(201).json({
      result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const getTeacher = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await Teacher.query().findOne({ user: id });
    if (!result) {
      const err = { code: 404, message: 'Teacher not found' };
      throw err;
    }
    res.status(200).json({
      teacher: result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const delTeacher = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await Teacher.query()
      .delete()
      .where('user', id);
    if (result === 0) {
      const err = { code: 404, message: 'Teacher not found' };
      throw err;
    }
    res.status(200).json({
      result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const getStudents = async (req, res) => {
  try {
    const result = await Student.query();
    res.status(200).json({
      students: result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const addStudent = async (req, res) => {
  try {
    const { student } = req.body;
    const result = await Student.query().insert(student);
    res.status(201).json({
      result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const getStudent = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await Student.query().findOne({ user: id });
    if (!result) {
      const err = { code: 404, message: 'Student not found' };
      throw err;
    }
    res.status(200).json({
      student: result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const delStudent = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await Student.query()
      .delete()
      .where('user', id);
    if (result === 0) {
      const err = { code: 404, message: 'Student not found' };
      throw err;
    }
    res.status(200).json({
      result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

export {
  getTeachers,
  addTeacher,
  getTeacher,
  delTeacher,
  getStudents,
  addStudent,
  getStudent,
  delStudent
};
