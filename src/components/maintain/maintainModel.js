import { Model } from 'objection';

class Teacher extends Model {
  static get tableName() {
    return 'teachers';
  }
}

class Student extends Model {
  static get tableName() {
    return 'students';
  }
}

export { Teacher, Student };
