import { Router } from 'express';
import {
  getTeachers,
  addTeacher,
  getTeacher,
  delTeacher,
  getStudents,
  addStudent,
  getStudent,
  delStudent
} from './maintainService';

const router = Router();

router.get('/teacher', getTeachers);
router.post('/teacher', addTeacher);
router.get('/teacher/:id', getTeacher);
router.delete('/teacher/:id', delTeacher);
router.get('/student', getStudents);
router.post('/student', addStudent);
router.get('/student/:id', getStudent);
router.delete('/student/:id', delStudent);

export default router;
