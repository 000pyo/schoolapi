import maintainController from './maintainController';
import { Teacher, Student } from './maintainModel';

export default maintainController;
export { Teacher, Student };
