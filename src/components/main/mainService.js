import log4js from 'log4js';
import { transaction } from 'objection';
import { Record } from './mainModel';
import { Student, Teacher } from '../maintain';

const logger = log4js.getLogger();

const getErrorMessage = err => {
  const { message, code, sqlMessage } = err;
  logger.error(code, message);

  let rescode = code;
  let resmessage = message;
  logger.debug(code);
  switch (code) {
    case 'ER_BAD_FIELD_ERROR':
      rescode = 400;
      resmessage = sqlMessage;
      break;
    case 'ER_DUP_ENTRY':
      rescode = 500;
      resmessage = sqlMessage;
      break;
    case 'ER_NO_REFERENCED_ROW_2':
      rescode = 404;
      resmessage = sqlMessage;
      break;
    case 400:
    case 404:
      break;
    default:
      rescode = 500;
      break;
  }

  return { code: rescode, message: resmessage };
};

const getRecords = async (req, res) => {
  try {
    const result = await Record.query();
    res.status(200).json({
      records: result
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const register = async (req, res) => {
  try {
    const { teacher, students } = req.body;

    if (!teacher || !students || students.length === 0) {
      const errorobj = { code: 400, message: 'Invalid body' };
      throw errorobj;
    }

    const insertItems = students.map(student => ({ teacher, student }));
    let trx;
    try {
      trx = await transaction.start(Record.knex());

      const trans = insertItems.map(async item => {
        try {
          const insert = await Record.query(trx).insert(item);
          return insert;
        } catch (err2) {
          throw err2;
        }
      });

      const result = await Promise.all(trans);
      logger.debug('result', result);
      await trx.commit();

      res.status(204).json({});
    } catch (trxerr) {
      await trx.rollback();
      throw trxerr;
    }
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const getCommonStudents = async (req, res) => {
  try {
    const { teacher } = req.query;
    if (!teacher) {
      const errobj = { code: 400, message: 'No teacher in request' };
      throw errobj;
    }
    const isArray = Array.isArray(teacher);
    let action;

    if (!isArray) {
      action = Record.query().where('teacher', teacher);
    } else {
      const count = teacher.length;
      action = Record.query();
      teacher.forEach(item => {
        action.orWhere('teacher', item);
      });
      action
        .groupBy('student')
        .count('student as count')
        .select('student')
        .having('count', '=', count);
    }

    action.joinRelation('st').whereNot('st.status', 'suspended');

    const result = await action;

    const students = result.map(item => item.student);

    res.status(200).json({
      students
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const suspend = async (req, res) => {
  try {
    const { student } = req.body;

    if (!student) {
      const errorobj = { code: 400, message: 'Invalid body' };
      throw errorobj;
    }

    const result = await Student.query()
      .where({ user: student })
      .patch({ status: 'suspended' });

    if (result === 0) {
      const errorobj = { code: 404, message: 'Student does not exsit' };
      throw errorobj;
    }

    res.status(204).json({});
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

const retNotification = async (req, res) => {
  try {
    const { teacher, notification } = req.body;

    const regex = /@[a-zA-Z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/g;

    if (!teacher || !notification) {
      const errorobj = { code: 400, message: 'Invalid body' };
      throw errorobj;
    }

    const mentionedStudents = regex.exec(notification);

    const tcExist = await Teacher.query()
      .where({ user: teacher })
      .limit(1);

    if (tcExist.length === 0) {
      const errorobj = { code: 404, message: 'Teacher not exist' };
      throw errorobj;
    }

    const result = await Record.query()
      .where({ teacher })
      .joinRelation('st')
      .select('student', 'st.status');

    const students = new Map();
    result.forEach(item => {
      students.set(item.student, item.status);
    });

    if (mentionedStudents) {
      mentionedStudents.forEach(item => {
        const student = item.substr(1);
        const exist = students.get(student);
        if (!exist && exist !== 'suspended') {
          students.set(student, '');
        }
      });
    }

    const recipients = [...students.keys()];

    res.status(200).json({
      recipients
    });
  } catch (err) {
    const { code, message } = getErrorMessage(err);
    res.status(code).json({
      message
    });
  }
};

export { getRecords, register, getCommonStudents, suspend, retNotification };
