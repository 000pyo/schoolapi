import { Router } from 'express';
import {
  getRecords,
  register,
  getCommonStudents,
  suspend,
  retNotification
} from './mainService';

const router = Router();

router.get('/record', getRecords);
router.post('/register', register);
router.get('/commonstudents', getCommonStudents);
router.post('/suspend', suspend);
router.post('/retrievefornotifications', retNotification);

export default router;
