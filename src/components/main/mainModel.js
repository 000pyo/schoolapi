import { Model } from 'objection';
import { Teacher, Student } from '../maintain';

class Record extends Model {
  static get tableName() {
    return 'records';
  }

  static get relationMappings() {
    return {
      tc: {
        relation: Model.BelongsToOneRelation,
        modelClass: Teacher,
        join: {
          from: 'records.teacher',
          to: 'teachers.user'
        }
      },
      st: {
        relation: Model.BelongsToOneRelation,
        modelClass: Student,
        join: {
          from: 'records.student',
          to: 'students.user'
        }
      }
    };
  }
}

// eslint-disable-next-line import/prefer-default-export
export { Record };
