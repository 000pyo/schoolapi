import express from 'express';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import log4js from 'log4js';
import swaggerUI from 'swagger-ui-express';
import YAML from 'yamljs';
import { createServer } from 'http';

import indexRoute from './routes';
import initmodel from './util/dbutil';

const app = express();
const server = createServer(app);

const logger = log4js.getLogger();
logger.level = 'debug';

initmodel();

app.use(helmet());
app.use(cors());
if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('dev'));
}
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api', indexRoute);
if (process.env.NODE_ENV !== 'production') {
  const swaggerDocument = YAML.load('swagger.yaml');
  app.use('/', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
}

// Start listening API web server.
const port = process.env.PORT || 8081;

server.listen(port, () => {
  logger.info(`config-api running at port ${port}`);
});

module.exports = app;
